# Matplotlib 数据绘图基础入门

| 作者：huhuhang | 难度：初级 | 综合评分：:star:9.5 |
|:-----------:|:-----:|:--------------:|

Matplotlib 是支持 Python 语言的开源绘图库，因为其支持丰富的绘图类型、简单的绘图方式以及完善的接口文档，深受 Python 工程师、科研学者、数据工程师等各类人士的喜欢。本课程主要对 Matplotlib 常用的绘图方法进行简介。并介绍其绘图常用的方法。

## 你将学到的

| :heavy_check_mark: Matplotlib 简介    | :heavy_check_mark: 二维图的绘制  |
|:-----------------------------------:|:--------------------------:|
| :heavy_check_mark: 三维图的绘制           | :heavy_check_mark: 子图及组合图形 |
| :heavy_check_mark: 兼容 MATLAB 风格 API |                            |

## 课程内容

1. Matplotlib 二维图形绘制方法
   
   - 二维图形绘制
   
   - 子图及组合图形
   
   - 兼容 MATLAB 风格 API

2. Matplotlib 三维图形绘制方法
   
   - 三维图形绘制
   
   - 三维混合图
   
   - 三维子图
