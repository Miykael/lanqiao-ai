# 循环神经网络基础入门

<div align="center">
  <table>
    <tr>
      <td>作者: 酚酞酒</td>
      <td>难度: 初级</td>
      <td>综合评分: ★9.2</td>
    </tr>
  </table>
</div>

本课程通过讲解循环神经网络 RNN 基础算法及其变体算法，使你了解 RNN 基本算法的拓扑结构、循环方式，以及 RNN 变体的细胞状态和细胞状态的变化过程。

## 你将学到的

<div align="center">
  <table>
    <tr>
      <td>✔ RNN 基本原理</td>
      <td>✔ LSTM 基本原理</td>
    </tr>
    <tr>
      <td>✔ GRU 基本原理</td>
      <td>✔ RNN 的构建与训练</td>
    </tr>
  </table>
</div>

## 课程内容

1. RNN 神经网络及其变体网络
   - 循环网络的概念
   - RNN 网络结构
   - LSTM 网络结构
   - 细胞体三个门的工作方式
   - 其他 LSTM 网络变体的结构
2. 利用 RNN 进行简单的加法运算
   - RNN 神经网络及其变体的基本原理
   - Python 的基本用法
   - NumPy 库的基本使用
