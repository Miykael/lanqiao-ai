# SciPy 科学计算基础入门

| 作者：huhuhang | 难度：初级 | 综合评分：:star:9.5 |
| ----------- | ----- | -------------- |

Scipy 是一个用于数学、科学和工程的开源库，其集成了统计、优化、线性代数、傅立叶变换、信号和图像处理，ODE 求解器等模块，是使用 Python 进行科学计算的重要工具之一。本课程将带你了解 SciPy 的基础用法。

## 你将学到的

| :heavy_check_mark: 插值函数 | :heavy_check_mark: 图形处理 |
|:-----------------------:|:-----------------------:|
| :heavy_check_mark: 优化方法 | :heavy_check_mark: 信号处理 |
| :heavy_check_mark: 统计函数 | :heavy_check_mark: 稀疏矩阵 |
