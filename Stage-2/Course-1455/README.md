# 人工神经网络基础入门

| 作者：小强_同学 | 难度：初级 | 综合评分：:star:7.6 |
|:--------:|:-----:|:--------------:|

本课程将从底层的角度讲解神经网络的正向传播原理、反向传播原理以及他们的实现过程。最后利用 NumPy 手写一个完整的浅层神经网络模型，并使用这个模型对手写字符进行识别。

## 你将学到的

| :heavy_check_mark: 正向传播   | :heavy_check_mark: 梯度下降算法 |
|:-------------------------:|:-------------------------:|
| :heavy_check_mark: 反向传播原理 | :heavy_check_mark: 手写字符识别 |
