# PyTorch 深度学习基础入门

| 作者：huhuhang | 难度：初级 | 综合评分：:star:9.7 |
|:-----------:|:-----:|:--------------:|

PyTorch 是由 Facebook 主导开发的深度学习框架，因其高效的计算过程以及良好的易用性被诸多大公司和科研人员所喜爱。本次课程中，我们将学习 PyTorch 的基础语法，了解 Autograd 自动求导机制，并最终利用 PyTorch 构建可用于图像分类任务的人工神经网络。

## 你将学到的

| :heavy_check_mark: PyTorch 基础语法 | :heavy_check_mark: Autograd 自动求导 |
|:------------------------------- |:-------------------------------- |
| :heavy_check_mark: 人工神经网络       | :heavy_check_mark: 图像分类任务        |
