# TensorFlow 2 新特性快速入门

| 作者：huhuhang | 难度：初级 | 综合评分：:star:9.5 |
|:-----------:|:-----:|:--------------:|

TensorFlow 2 已正式推出，根据 TensorFlow 官方介绍，2.0 版本将专注于简洁性和易用性的改善。本次课程将带你了解 TensorFlow 2.0 的新特性，并完成快速入门和过渡。

## 课程内容

1. TensorFlow 2.x 新增变化特性
   
   1. 安装 TensorFlow 2.x
   
   2. EagerExecution
   
   3. Keras (Tensorflow Backend)

2. TensorFlow 2.x 实现线性回归
   
   1. 低阶 API 实现
   
   2. 高阶 API 实现
   
   3. TensorFlow 1.x 实现

3. TensorFlow 2.x 构建神经网络
   
   1. 低阶 API 构建
   
   2. Keras 高阶 API 实现
